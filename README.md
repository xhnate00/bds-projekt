#bpc-bds project


#uvod
Cílem tohoto projektu je vytvořit návrh databáze, znázornit ji v diagramu a implementovat v systémech SQL, přičemž jeden z nich má být PostgreSQL.
Cílem textu je pak popsat celou databázi, ukázat její funkčnost a přidat link na veřejně dostupný odkaz na GitLabu.
První projekt (návrh databáze) byl vytvořen ve spolupráci s Michalem Hnátkem (230559).

#popis databaze
Tato databáze autopůjčovny si archivuje záznamy svých automobilů a zákazníků. Pomocí dalších tabulek nadále získává podrobné informace o všech automobilech, ale i zákaznících. V databázi jsou taktéž evidováni zaměstnanci autopůjčovny, banky, které vydávají zákazníkům kreditní karty (kvůli relaci M:N) nebo jednoduchý formát pojištění auta. Každá zápůjčka auta si poté eviduje všechny informace o zapůjčeném autě i o zákazníkovi, který si auto zapůjčil.
Databáze celkově obsahuje šestnáct tabulek (nepočítáme-li mezi tabulku u relace M:N, tak patnáct). Většina relací v databázi se nachází ve formátu 1:1, u adres zaměstnanců a zákazníků se ale vyskytuje relace 1:N (neboť na jedné adrese může bydlet více lidí), u bank poté relace M:N, neboť jeden zákazník může mít více bankovních účtů (popř. kreditních karet) a zároveň banka může mít více zákazníků.

#třetí normální forma a popis tabulek
Naše databáze se nachází ve třetí normální formě. Snažili jsme se maximálně zefektivnit strukturu databáze, neduplikovat žádné sloupce v tabulkách, mít v každé tabulce co nejméně informací (a pouze ty nejnutnější) a co nejlépe využít vzájemné vztahy tabulek (např. napojení tabulky address hned na tři entity – adresa zákazníka, adresa zaměstnance a adresa předání automobilu).
Hlavní tři tabulky v naší databázi jsou car, customer a rent. Na tabulku car jsou poté navázány další tabulky, které popisují vlastnosti a stav automobilů, jedná se o tabulky status, brand, equipment a dále o engine a tuning.
Tabulku customer, která popisuje zákazníka, napojují tabulky bank (a mezitabulka kvůli relaci M:N customer_has_bank), credential a contact. Na tabulku rent jsou navázány jak primární klíče dvou hlavních entit – customer a car, tak i tabulka pojištění výpůjčky insurance nebo tabulka zaměstnance employee, který auto zapůjčil. Vícero využití mají poté tabulky name a contact, které jsou přiřazovány k osobám (ať už customer nebo employee). O jedno využití navíc má poté tabulka address, která navíc ještě poukazuje na místo předání auta v tabulce rent. Z návrhu je navíc patrné, že ne každé auto musí být pojištěné (ale např. pouze zastavené kreditní kartou) a že každé auto nemusí mít tuning.
